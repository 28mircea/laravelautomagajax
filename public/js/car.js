$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(document).ready(function (){
     
});

function createCar(){
    $.get('/cars/create', function(response){
        $('#carModal .modal-body').html(response);
    })
}


function storeCar(){
    var formData = new FormData();
    var images =$('input[type=file]')[0].files;
    
    formData.append('car_model', $('#car_model').val());
    formData.append('year', $('#year').val());
    formData.append('price', $('#price').val());
    formData.append('gearbox', $('#gearbox:checked').val());
    formData.append('emissions_class', $('#emissions_class').val());
    formData.append('service_manual', $('#service_manual:checked').val());
    formData.append('other_information', $('#other_information').val());	
	

    if ($('#car_model').val()== '' || $('#year').val()=='' || $('#gearbox:checked').val()=='' || $('#emissions_class').val()==''){
        $('#ajaxErrors ul').html('');
        $('#ajaxErrors ul').append('<li>Please fill the values!!</li>');
        return false;
    } 

    for(var i=0;i<images.length;i++){
        formData.append( 'images[]', $('#image')[0].files[i]);
    }    
    
    $.ajax({
        url: '/cars',
        method:'post',
        enctype: 'multipart/form-data',
        data: formData,
        processData: false,
        contentType: false
    }).done(function(response){            
        $('#carModal').modal('hide');
        ajaxcars();
    }).fail(function(response){
        console.log(response.responseJSON.errors);
        let errors = response.responseJSON.errors;
        $('#ajaxErrors ul').html('');
        $.each(errors,function(key, value){
            $('#ajaxErrors ul').append('<li>'+value[0]+'</li>');
        })
    })
    
}


function showCar(id){
    $.get('/cars/'+id, function(response){
        $('#carModal .modal-body').html(response);
    })
}



function editCar(id){
    $.get('/cars/'+id+'/edit-car/', function(response){
        $('#carModal .modal-body').html(response)
    });
}



function updateCar(id){
    var car_model = $('#car_model').val();
    var year = $('#year').val();
    var price = $('#price').val();
    var gearbox = $('#gearbox:checked').val();
    var emissions_class = $('#emissions_class').val();
    var service_manual = $('#service_manual:checked').val();
    var other_information = $('#other_information').val();
    $.ajax({
        url:'/cars/'+id+'/update-car/',
        method:'post',
        data:{
            car_model:car_model,
            year:year,
            price:price,
            gearbox:gearbox,
            emissions_class:emissions_class,
            service_manual:service_manual,
            other_information:other_information
        }
    }).done(function(response){
        $('#carModal').modal('hide');       
        ajaxEdit(response.car_id);
    })
}



function editImages(id){
    $.get('/cars/'+id+'/edit-images/', function(response){
        $('#carModal .modal-body').html(response)
    });
}



function updateImages(id){
    var formData = new FormData();     
    var images =$('input[type=file]')[0].files

 for(var i=0;i<images.length;i++){
       formData.append( 'images[]', $('#image')[0].files[i]);
   }     
    $.ajax({
        url: '/cars/'+id+'/update-images/',
        method:'post',
        enctype: 'multipart/form-data',
        data: formData,
        processData: false,
        contentType: false
    }).done(function(response){
        $('#carModal').modal('hide');
        ajaxEdit(id);
    })
}



function destroyOnePicture(id){
    $.ajax({        
        url:'/cars/destroy-onepicture/'+id,
        method:'get',
        data:{
            id:id
        }
    }).done(function(response){
        $('#carModal').modal('hide'); 
        ajaxEdit(response.car_id);
       
    })
}



function destroyCar(id){
    $.ajax({
        url:'/cars/'+id,
        method:'delete',
        data:{
            id:id
        }
    }).done(function(response){
        $('.car'+ $('.id').text()).remove();
        ajaxcars();
    })
}



function ajaxcars(){
    $.get('/ajax/cars', function(response){
        $('#carTable').html(response);
    })
}


function ajaxEdit(id){
    $.get('/ajax/cars/'+id+'/edit/', function(response){
        $('#imagesTable').html(response);
    })
}


function resetCar(){
    $('input').val('');
    $('#car_model').val();    
    $('#startyear').val(0);
    $('#endyear').val(0);
    $('#startprice').val('(from)');
    $('#endprice').val('(to)');
    $('input[name="gearbox"]').prop('checked', false);
    $('#emissions_class').val(0);
    $('#service_manual').prop('checked', false);
    $('textarea').val('');
    $('#other_information').val('');
}


function resultsCar(){
    var car_model = $('#car_model').val();
	var startyear = $('#startyear').val();
	var endyear = $('#endyear').val();
	var startprice = $('#startprice').val();
	var endprice = $('#endprice').val();
	var gearbox = $("[type='radio']:checked").val();	   
	var emissions_class = $('#emissions_class').val();
	var service_manual = $('#service_manual:checked').val();
    var other_information = $('#other_information').val();
    
    if(car_model=="" && startyear=="" && endyear=="" && startprice=="" && endprice=="" && gearbox=="" && emissions_class=="" && service_manual=="" && other_information==""){
        $('#carTable tbody').empty();
        $('#carTable').show();
        $('#carTable tbody').append("<tr class='car'>"+
        "<td colspan='4'>" + "No results found!" + "</td>"+
        "<tr>");
    }
    else{
        $.get('/search',{
            car_model:car_model,
            startyear:startyear,
            endyear:endyear,            
            startprice:startprice,
            endprice:endprice,
            gearbox:gearbox,
            emissions_class:emissions_class,
            service_manual:service_manual,
            other_information:other_information
        }, function(response){            
            //$('#carTable tbody').empty();
            //$('#carTable').show();
            //$('#carFormSearch').hide();
            $('#carSearch').empty().html(response);
            //$('#carFormSearch').show();
            //$('#carTable').show();            
           
        });
    }
}





