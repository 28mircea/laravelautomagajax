@extends('layouts.app')
@section('content')
<div class="container">
<h1 class="text-center">Laravel-AUTOMAG-Ajax</h1>    
@if ($errors->any())
    <div class="alert alert-danger">
    	<ul>	    	
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
    <div id="carFormSearch" class="text-center border border-light p-5">    
            <table id="tablesearch" class="table">
                <tr>
                    <td><label for="car_model">Car Model</label></td>
                    <td><input type="text" name="car_model" id="car_model" placeholder="introduce car model"></td>
                </tr>
                <tr>
                    <td><label for="year">Year</label></td>                
                    <td>
                        <select name="startyear" id="startyear">
                            <option value="0" selected="selected">(from year)</option>
                                
                                    
                                    @for ($startyear=date('Y'); $startyear>=1960; $startyear--)                                
                                        <option value="{{ $startyear }}">{{ $startyear }}</option>
                                    @endfor   
                                
                        </select>    
                    </td>
                    <td>
                        <select name="endyear" id="endyear">
                            <option value="0" selected="selected">(to year)</option>
                                
                                    
                                    @for ($endyear=date('Y'); $endyear>=1960; $endyear--)                                
                                        <option value="{{ $endyear }}">{{ $endyear }}</option>
                                    @endfor   
                                
                        </select>    
                    </td>                      
                    
                </tr>
                <tr>
                    <td><label for="price">Price</label></td>
                    <td><input type="number" name="startprice" id="startprice" placeholder=(from)></td>
                    <td><input type="number" name="endprice" id="endprice" placeholder=(to)></td>
                </tr>
                <tr>
                    <td><label for="gearbox">Gearbox Type</label></td>
                    <td>    
                        <input type="radio" name="gearbox" class ="mr-1" value="Manual" @if (old('gearbox') == 'Manual')  checked @endif >Manual
                        <input type="radio" name="gearbox" class ="mr-1" value="Automatic" @if (old('gearbox') == 'Automatic')  checked @endif >Automatic
                    </td>
                </tr>
                <tr>
                    <td><label for="emissions_class" class="col-md-4 col-form-label text-md-right">Emissions Class</label></td>
                    <td>
                        <select name="emissions_class" id="emissions_class">
                        <option value="0" selected="selected">(select emissions)</option>
                                                <option value="Euro1">Euro1</option>
                                                <option value="Euro2">Euro2</option>
                                                <option value="Euro3">Euro3</option>
                                                <option value="Euro4">Euro4</option>
                                                <option value="Euro5">Euro5</option>
                                                <option value="Euro6">Euro6</option>
                        </select>
                    </td>    
                </tr>
                <tr>
                    <td><label for="service_manual" class="col-form-label">Service Manual</label></td>
                    <td><input type="checkbox" name="service_manual" id="service_manual" value='service_manual'></td>
                </tr>
                <tr>
                    <td><label for="other_information">Other Information</label></td>
                    <td><textarea name="other_information" id="other_information" rows="5" class="form-control" placeholder="(other information)"></textarea></td>
                </tr>            
            <tr>
                <td><button class="btn btn-primary" onclick="resetCar()">Reset Search</button></td>        
                <td><button class="btn btn-primary" onclick="resultsCar()">Search Results</button></td>   
            </tr>
            </table>
    </div>
    <div class="row">
        <div class="center-block">
            
        </div>
        <div id="carTable" class="center-block table-responsive">
            <table class="table table-striped">
                <thead>
                
                </thead>
                <tbody id="carSearch">
                
                </tbody>
            </table>
        </div>
    </div>     
</div>
@endsection     


