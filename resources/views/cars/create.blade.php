<h1>Add Car</h1>

    <div class="alert alert-danger" id="ajaxErrors">
    	<ul>	    	
            
        </ul>
    </div>

<div class="text-center border border-light p-5">
    <table class="table">
        <tr>
            <td><label for="car_model">Car Model</label></td>
            <td><input type="text" name="car_model" id="car_model" value="{{old('car_model')}}"></td>           
        </tr>
        <tr>
            <td><label for="year">Year</label></td>
            <td>
                <select name="year" id="year">
                    <option value="{{old('year')}}" selected="selected">{{old('year')}}</option>                       
							
                             @for ($year=date('Y'); $year>=1960; $year--)                                
                                <option value="{{ $year }}">{{ $year }}</option>
                             @endfor   
						
                </select>    
            </td>          
        </tr>
        <tr>
            <td><label for="price">Price</label></td>
            <td><input type="text" name="price" id="price" value="{{old('price')}}"></td>           
        </tr>
        <tr>
            <td><label for="gearbox">Gearbox type</label></td>
            <td>    
                <input type="checkbox" name="gearbox" id="gearbox" class ="mr-1" value='Manual'@if (old('gearbox') == 'Manual') checked @endif >Manual
                <input type="checkbox" name="gearbox" id="gearbox" class ="mr-1" value='Automatic'@if (old('gearbox') == 'Automatic') checked @endif >Automatic
            </td>            
        </tr>
        <tr>
        <td><label for="emissions_class" class="col-md-4 col-form-label text-md-right">Emissions class</label></td>
            <td>
                <select name="emissions_class" id="emissions_class">
                <option value="{{old('emissions_class')}}" selected="selected">{{old('emissions_class')}}</option>
                                        <option value="Euro1">Euro1</option>
                                        <option value="Euro2">Euro2</option>
                                        <option value="Euro3">Euro3</option>
                                        <option value="Euro4">Euro4</option>
                                        <option value="Euro5">Euro5</option>
                                        <option value="Euro6">Euro6</option>
                </select>
            </td>          
        </tr>
        <tr>
            <td><label for="service_manual" class="col-form-label">Service Manual</label></td>
            <td><input type="checkbox" name="service_manual" id="service_manual" value='service_manual' @if (old('service_manual') == 'service_manual') checked @endif></td>
        </tr>
        <tr>
            <td><label for="other_information">Other Information</label></td>
            <td><textarea name="other_information" id="other_information" rows="5" class="form-control" placeholder="(other information)">{{ old('other_information') }}</textarea></td>
        </tr>
        <tr>
            <td><input type="file" name="images[]" id="image" multiple="multiple"></td>
            <td><small id="fileHelp" class="form-text text-muted">Please upload a valid image file.</small></td>            
        </tr>        
        <tr>
            <td colspan="2"><button class="btn btn-primary" onclick="storeCar()">Save Car</button></td>
        </tr>
    </table>  
</div>

