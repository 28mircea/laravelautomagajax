@extends('layouts.app')
@section('content')
<div class="container">
    <br />
    <h1 align="center">Laravel - CRUD Ajax Car-MultiPicture</h1>    
    <br /><br />
    <div id="carTable" class="table-responsive">
      <table class="table table-striped">
        <thead>
          <th class="text-center">Car ID</th>
          <th class="text-center">Car Model</th>          
          <th class="text-center">Image</th>          
          <th class="text-center" >Actions</th>
        </thead>
        <tbody>
          @foreach($cars as $car)
            <tr class="car{{$car->id}}">        
              <td class="text-center">{{$car->id}}</td>
              <td class="text-center">{{$car->car_model}}</td>
              <td align="center">                
                  <div id="carouselExampleControls" class="carousel slide" data-interval="false" data-ride="carousel">
                      <div class="carousel-inner" role="listbox">
                      @foreach( $car->carphotos as $carphoto )    
                          <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                                <img class="d-block img-fluid" src="{{asset('storage/images/'.$carphoto->image)}}" width="200">             
                          </div>
                      @endforeach
                          <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                            <span><i class="fa fa-angle-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                          </a>
                          <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                            <span><i class="fa fa-angle-right" aria-hidden="true"></i></span>
                            <span class="sr-only">Next</span>
                          </a>  
                      </div>
                  </div>
              </td>          
              <td class="text-center">                   
                  <button class="btn btn-info" data-toggle="modal" data-target="#carModal" onclick="showCar({{$car->id}})">Show Car</button>                                         
              </td> 
            </tr>
          @endforeach     
        </tbody>
    </table>    
  </div>
  <div class="row">
      <div class="col-12 text-center">
          {{ $cars->links() }}
      </div>
  </div> 
@endsection
