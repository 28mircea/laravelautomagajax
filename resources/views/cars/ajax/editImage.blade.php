
<table class="table" id="imagesTable">
        @foreach( $car->carphotos->chunk(4)  as $chunk )
            <tr>
                @foreach($chunk as $carphoto) 
                    <td>
                        <img src="{{asset('storage/images/'.$carphoto->image)}}" style="width:150px;">
                    </td>
                @endforeach                            
            </tr>                      
        @endforeach
    <tr>
        <td><input type="file" multiple="multiple" name="image[]" id="image"></td>
    </tr>        
    <tr>
        <td colspan="2"><button class="btn btn-primary" onclick="updateImages({{$car->id}})">Update Images</button></td>
    </tr>
</table>  



