<table class="table">
    <tr>
        <th class="text-center">Car ID</th>
        <th class="text-center" >Car Model</th>                
        <th class="text-center">Image</th>        
        <th class="text-center">Actions</th>
    </tr>
    @foreach($cars as $car)
    <tr class="car{{$car->id}}">
        <td class="text-center">{{$car->id}}</td>
        <td class="text-center">{{$car->car_model}}</td>                
        <td align="center">
            <ol class="carousel-indicators">
                @foreach( $car->carphotos as $carphoto )
                    <li data-target="#carouselExampleIndicators" data-slide-to="{{ $loop->index }}" class="{{ $loop->first ? 'active' : '' }}"></li>
                @endforeach
            </ol> 

            <div id="carouselExampleControls" class="carousel slide" data-interval="false" data-ride="carousel">
                <div class="carousel-inner" role="listbox">
                @foreach( $car->carphotos as $carphoto )    
                    <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                        <img class="d-block img-fluid" src="{{asset('storage/images/'.$carphoto->image)}}" width="200">             
                    </div>
                @endforeach
                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span><i class="fa fa-angle-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span><i class="fa fa-angle-right" aria-hidden="true"></i></span>
                    <span class="sr-only">Next</span>
                    </a>  
                </div>
            </div>
        </td>
        <td class="text-center">
            <a href ="{{route('cars.edit',$car->id)}}" class="btn btn-primary">Edit Car</a>
            <br /><br />
            <button class="btn btn-info" data-toggle="modal" data-target="#carModal" onclick="showCar({{$car->id}})">Show Car</button>
            <br /><br />
            <button class="btn btn-danger" onclick="destroyCar({{$car->id}})">Delete Car</button>
        </td>
    </tr>
    @endforeach
</table>


