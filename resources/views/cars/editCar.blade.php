<div class="container">
	<h1>Edit Car</h1>
    <div class="text-center border border-light p-5">
        <table class="table">
            <tr>
                <td><label for="car_model">Car Model</label></td>
                <td><input type="text" name="car_model" id="car_model" value="{{$car->car_model}}"></td>
            </tr>
            <tr>
                <td><label for="year">Year</label></td>
                <td>
                    <select name="year" id="year">
                        <option value="{{$car->year}}" selected="selected">{{$car->year}}</option>
                            
                                
                                 @for ($year=date('Y'); $year>=1960; $year--)                                
                                    <option value="{{ $year }}">{{ $year }}</option>
                                 @endfor   
                            
                    </select>    
                </td>
            </tr>
            <tr>
                <td><label for="price">Price</label></td>
                <td><input type="number" name="price" id="price" value="{{$car->price}}"></td>
            </tr>
            <tr>
                <td><label for="gearbox">Gearbox type</label></td>
                <td>    
                    <input type="checkbox" name="gearbox" id="gearbox" class ="mr-1" value='Manual'@if ($car->gearbox == 'Manual') checked @endif >Manual
                    <input type="checkbox" name="gearbox" id="gearbox" class ="mr-1" value='Automatic'@if ($car->gearbox == 'Automatic') checked @endif >Automatic
                </td>
            </tr>
            <tr>
            <td><label for="emissions_class" class="col-md-4 col-form-label text-md-right">Emissions class</label></td>
                <td>
                    <select name="emissions_class" id="emissions_class">
                    <option value="{{$car->emissions_class}}" selected="selected">{{$car->emissions_class}}</option>
                                            <option value="Euro1">Euro1</option>
                                            <option value="Euro2">Euro2</option>
                                            <option value="Euro3">Euro3</option>
                                            <option value="Euro4">Euro4</option>
                                            <option value="Euro5">Euro5</option>
                                            <option value="Euro6">Euro6</option>
                    </select>
                </td>    
            </tr>
            <tr>
            <tr>
                <td><label for="service_manual" class="col-form-label">Service Manual</label></td>
                <td><input type="checkbox" name="service_manual" id="service_manual" value='service_manual' 
                    @if ($car->service_manual == 'service_manual') checked @endif></td>
            </tr>
            <tr>
                <td><label for="other_information">Other Information</label></td>
                <td><textarea name="other_information" id="other_information" rows="5" class="form-control">{{$car->other_information}}</textarea></td>
            </tr>        
            <tr>
                <td colspan="2"><button class="btn btn-primary" onclick="updateCar({{$car->id}})">Update Car</button></td>
            </tr>
        </table>  
</div>

