<div class="container">
<h1 align="left">Show Car</h1>    
<br /><br />
    <div id="carTable" class="table-responsive">
        <table class="table">
            <tr>
                <td colspan="8" align="center">
                    <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        @foreach( $car->carphotos as $carphoto )
                            <li data-target="#carouselExampleIndicators" data-slide-to="{{ $loop->index }}" class="{{ $loop->first ? 'active' : '' }}"></li>
                        @endforeach
                    </ol> 
                        <div class="carousel-inner">
                            <div class="carousel-inner" role="listbox">
                                @foreach( $car->carphotos as $carphoto )    
                                    <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                                        <img class="d-block img-fluid" src="{{asset('storage/images/'.$carphoto->image)}}" width="600">             
                                    </div>
                                @endforeach
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div> 
                </td>        
            </tr>
            <tr>
                <th class="text-center">Car ID</th>
                <th class="text-center">Car Model</th>
                <th class="text-center">Year</th>
                <th class="text-center">Price</th>
                <th class="text-center">Gearbox Type</p>
                <th class="text-center" >Emissions Class</th>        
                <th class="text-center" >Service Manual</th>
                <th class="text-center">Other Information</th>                   
            </tr>            
            <tr class="car{{$car->id}}">
                <td>{{$car->id}}</td>
                <td>{{$car->car_model}}</td>
                <td>{{$car->year}}</td>
                <td>{{$car->price}}</td>
                <td>{{$car->gearbox}}</td>
                <td>{{$car->emissions_class}}</td>
                <td>{{$car->service_manual}}</td>
                <td>{{$car->other_information}}</td>               
            </tr>
           
        </table>
    </div>
</div>