<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('search');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('cars','CarController');
Route::get('/ajax/cars','CarController@ajaxIndex');

Route::get('/mycars','CarController@mycars')->name('mycars');

Route::get('/cars/{car}/edit-car','CarController@editCar');
Route::post('/cars/{car}/update-car','CarController@updateCar');

Route::get('/cars/{car}/edit-images','CarController@editImages');
Route::post('/cars/{car}/update-images','CarController@updateImages');

Route::get('/ajax/cars/{car}/edit','CarController@ajaxEdit');

Route::get('/cars/destroy-onepicture/{image}','CarController@destroyOnePicture');

Route::get('/search','CarController@processCar');
