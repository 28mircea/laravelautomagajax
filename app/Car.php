<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    protected $fillable=['car_model','year','price','gearbox','emissions_class','service_manual','other_information','user_id'];

    public function user(){
        return $this->belongsTo('App\User');
    }
    
    public function carphotos(){
        return $this->hasMany('App\CarPhoto');
    }
}
