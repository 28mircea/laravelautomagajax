<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarPhoto extends Model
{
    protected $fillable = ['image','car_id'];

    public function car(){
        return $this->belongsTo('App\Car');
    }  
}
