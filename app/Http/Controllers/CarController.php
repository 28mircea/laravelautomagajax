<?php

namespace App\Http\Controllers;

use App\Car;
use App\CarPhoto;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Input;
use Illuminate\Support\Facades\Validator;

use Illuminate\Http\Request;

class CarController extends Controller
{
    
    public function __construct()
    {
        return $this->middleware('auth')->except(['processCar','show']);       
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cars = Car::orderBy('created_at','desc')->paginate(3);
        return view('cars.index',compact('cars'));
    }


    public function ajaxIndex()
    {
        $cars = Car::all();
        return view('cars.ajax.index',compact('cars'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cars.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validatedData = $request->validate([
            'car_model' => 'required|unique:cars',
            'year' => 'required',
            'price' => 'required|numeric',
            'gearbox' => 'required',
            'emissions_class' => 'required',
        ]);

        if ($validatedData){
			
            $car=new Car();
            $car->car_model = $request->car_model;
            $car->year = $request->year;
            $car->price = $request->price;
            $car->gearbox = $request->gearbox;
            $car->emissions_class = $request->emissions_class;
            $car->service_manual = $request->service_manual;
            $car->other_information = $request->other_information;
            $car->user_id = \Auth::id();

                if($request->images){            
                    foreach ($request->images as $image){
                        if($image){
                            $request->validate([
                                'images' => 'required',
                                'images.*' => 'mimes:bmp,jpeg,png,jpg,svg|max:2048'
                            ]);
        
                            $ext = $image->getClientOriginalExtension();
                            $file = date('YmdHis').rand(1,99999).'.'.$ext;            
                            $filepath = $image->storeAs('public/images',$file);
                        }              
                        
                        $car->save();
                        $carphoto = new CarPhoto();            
                        $carphoto->car_id = $car->id;
                        $carphoto->image = $file;
                        $carphoto->save();
                }
            }            
            else{
                $car->save();
            }
        }

        return $car;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Car $car)
    {
        return view('cars.show',compact('car'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function mycars()
    {
        $cars = Car::where('user_id', auth()->user()->id)->paginate(3);
        
        return view('cars.mycars',['cars'=>$cars]);
    }



    public function edit(Car $car)
    {
        return view('cars.edit', compact('car'));
    }

    
    public function ajaxEdit(Car $car)
    {
        return view('cars.ajax.edit',compact('car'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function updateCar(Request $request, Car $car)
    {
        $car->car_model = $request->car_model;
        $car->year = $request->year;
        $car->price = $request->price;
        $car->gearbox = $request->gearbox;
        $car->emissions_class = $request->emissions_class;
        $car->service_manual = $request->service_manual;
        $car->other_information = $request->other_information;        
        $car->save(); 
        
        return $car;
    }

    

    public function editImages(Car $car){
        
        return view('cars.editImages',compact('car'));
    }


    
    protected function updateImages(Request $request, Car $car)
    {        
        
        if($request->hasFile('images')){
            foreach ($request->images as $image){
                                
                $ext = $image->getClientOriginalExtension();
                $filename = date('YmdHis').rand(1,99999).'.'.$ext;            
                $filepath = $image->storeAs('public/images',$filename);

                $carphoto = new CarPhoto();
                $carphoto->image = $filename;
                $carphoto->car_id = $car->id;
                $carphoto->save();
            }
        }
        
       
        return "Update Images";           
        
    }



    public function processCar(Request $request)
    {                
        $car_model = $request->input('car_model');
        $words = explode(" ",$car_model);

        $startyear = $request->input('startyear');       
        $endyear = $request->input('endyear');  
        $startprice = $request->input('startprice');       
        $endprice = $request->input('endprice');
        $gearbox = $request->input('gearbox');
        $emissions_class = $request->input('emissions_class');
        $service_manual = $request->input('service_manual');
        $other_information = $request->input('other_information'); 

                        $query = Car::where('id','>',0);
                        
                        foreach($words as $word){
                            $query->where('car_model', 'like', "%$word%");
                        }
        
                        if ($startyear && $endyear){
                            $query->whereBetween('year', array($startyear, $endyear));
                        }
                        
                        if($startprice && $endprice){
                            $query->whereBetween('price', array($startprice, $endprice));
                        }
                        if($gearbox){
                            $query->where('gearbox','like',"%$gearbox%");
                        }
                        if($emissions_class){
                            $query->where('emissions_class','like',"%$emissions_class%");
                        }
                        if($service_manual){
                            $query->where('service_manual','like',"%$service_manual%");
                        }
                        if($other_information){
                            $query->where('other_information','like',"%$other_information%");
                        }
                        
        //$cars = $query->orderBy('created_at','desc')->paginate(2);
        $cars = $query->get();             
        return view('cars.result',compact('cars')); 
        //return $cars;              
    }



    public function destroyOnePicture($id){
        
        $carphoto = CarPhoto::find($id);        
        Storage::delete('public/images/'.$carphoto->image);       
        $carphoto->delete();        
        return $carphoto; 
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Car $car)
    {
        foreach( $car->carphotos as $carphoto ){
            Storage::delete('public/images/'.$carphoto->image);
        }
        $car->delete();
        return $car;
    }
    
}
